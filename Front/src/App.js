import React, { useState } from 'react';
import './App.css';


function App() {
  const [numDays, setNumDays] = useState('');
  const [symbol, setSymbol] = useState('');
  const [sentimentData, setSentimentData] = useState({});
  const [isLoading, setIsLoading] = useState(false);
  const [numArticles, setNumArticles] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    try {
      const response = await fetch(`http://localhost:5000/sentiment/${numDays}/${symbol}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
        },
      });
      const data = await response.json();
      setSentimentData(data);
      setNumArticles(data.LiczbaWiadomosci);
    } catch (error) {
      console.error('Błąd pobierania danych:', error);
    }

    setIsLoading(false);
  };

  return (
    <div className="App">
      <h1>Sentiment Analysis</h1>
      <form onSubmit={handleSubmit}>
        <label>
          Ilość dni:
          <input
            type="number"
            value={numDays}
            onChange={(e) => setNumDays(e.target.value)}
            required
          />
        </label>
        <label>
          Symbol:
          <input
            type="text"
            value={symbol}
            onChange={(e) => setSymbol(e.target.value)}
            required
          />
        </label>
        <button type="submit" disabled={isLoading}>
          {isLoading ? 'Ładowanie...' : 'Pobierz Sentiment Analysis'}
        </button>
      </form>

      {numArticles !== null && (
        <p>Znaleziono {numArticles} wiadomości.</p>
      )}

      {sentimentData && (
        <div>
          <h2>Wyniki analizy sentymentu</h2>
          <p>Apliacja analizuje pobiera wiadomości giełdowe i ocenia ja</p>
          <p>Sprawdź wiadomości o Microsoft wpisują symbol MSFT lub Google wpisując symbol GOOG</p>
          <p>Wyniki pojawią mogą pojawić się nawet po minucie, zalerzy to od ilości wiadomości</p>
          <p>Ta wersja ma ograniczenie na 100 zapytań</p>
          <p>Pozytywne: {sentimentData.Pozytywne}</p>
          <p>Negatywne: {sentimentData.Negatywne}</p>
          <p>Neutralne: {sentimentData.Neutralne}</p>
        </div>
      )}
    </div>
  );
}

export default App;
