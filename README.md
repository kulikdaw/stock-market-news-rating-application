# Stock Market News Analyzer

Welcome to the Stock Market News Analyzer! This application is designed to download and analyze stock market news using state-of-the-art AI models. It utilizes the FinBERT model for sentiment analysis and the News API for accessing news articles.

## Features

- Download and analyze stock market news articles
- Perform sentiment analysis on news articles using FinBERT
- Display sentiment analysis results for each news article
- Filter news articles based on specific stocks or keywords

## Prerequisites

Before getting started with the application, you need to obtain API keys for the following services:

- [Hugging Face](https://huggingface.co/ProsusAI/finbert): API key for accessing the FinBERT model
- [News API](https://newsapi.org/): API key for accessing news articles

## Configuration

1. Set up API keys:
- Obtain your API key from Hugging Face for the FinBERT model.
- Obtain your API key from News API for accessing news articles.
- Update the configuration file `main.py` with your API keys.

## Usage

1. Run the application:
2. Open your web browser and navigate to `http://localhost:5000` to access the application.

## Technologies Used

- React.js for the front-end development
- Flask for the back-end development
- FinBERT for sentiment analysis
- News API for accessing news articles

## License

This project is licensed for non-commercial use only. For commercial use, please contact the author.

## Author
Eng. David Kulik