import json
import requests
from flask import Flask, jsonify
from datetime import datetime, timedelta
from langchain.prompts.chat import (
    HumanMessagePromptTemplate,
    ChatPromptTemplate
)

app = Flask(__name__)
api_key_news = 'YOURAPI_NEWS API'       #YOUR api news api
API_URL = "https://api-inference.huggingface.co/models/ProsusAI/finbert"
headers = {"Authorization": "YOURAPI MODEL AI"}#YOUR api AI model

def query(payload):
    response = requests.post(API_URL, headers=headers, json=payload)
    return response.json()

def get_news_data(num_days, symbol):
    two_days_ago = (datetime.now() - timedelta(days=num_days)).strftime('%Y-%m-%d')
    url = f'https://newsapi.org/v2/everything?q={symbol}&from={two_days_ago}&sortBy=publishedAt&apiKey={api_key_news}'
    response = requests.get(url)
    commentary = []

    if response.status_code == 200:
        news_data = response.json()
        num_article = 0
        for article in news_data['articles']:
            commentary.append(article['description'])
            num_article += 1
        print('Odszukano: ', num_article)
        return commentary
    else:
        print(f"Błąd zapytania: {response.status_code}")
        return None

PROMPT_COM_INFO = """This message describes the company {symbol}, {com_information} """
messages = HumanMessagePromptTemplate.from_template(template=PROMPT_COM_INFO)
chat_prompt = ChatPromptTemplate.from_messages(messages=[messages])

@app.route('/sentiment/<int:num_days>/<symbol>', methods=['GET'])
def get_sentiment_results(num_days, symbol):
    commentary = get_news_data(num_days, symbol)
    if commentary is None:
        return jsonify({'error': 'Błąd pobierania danych wiadomości'})

    num_neutral_com = 0
    num_positive_com = 0
    num_negative_com = 0

    for i in range(0, len(commentary)):
        chat_prompt_with_values = chat_prompt.format_prompt(symbol=symbol, com_information=commentary[i])
        content = chat_prompt_with_values.messages[0].content

        output = query({"inputs": content})
        nested_result = output[0]
        neutral_score = next(item['score'] for item in nested_result if item['label'] == 'neutral')
        positive_score = next(item['score'] for item in nested_result if item['label'] == 'positive')
        negative_score = next(item['score'] for item in nested_result if item['label'] == 'negative')

        if neutral_score >= positive_score and neutral_score >= negative_score:
            num_neutral_com += 1
        elif positive_score > neutral_score and positive_score > negative_score:
            num_positive_com += 1
        else:
            num_negative_com += 1

    # Zwróć wyniki jako JSON
    return jsonify({
        'Pozytywne': num_positive_com,
        'Negatywne': num_negative_com,
        'Neutralne': num_neutral_com
    })

if __name__ == '__main__':
    app.run(debug=True)
